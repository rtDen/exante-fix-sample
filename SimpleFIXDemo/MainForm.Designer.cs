﻿namespace SimpleFIXDemo
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxStep1 = new System.Windows.Forms.GroupBox();
            this.labelShownCount = new System.Windows.Forms.Label();
            this.labelShown = new System.Windows.Forms.Label();
            this.progressBarLoadSymbols = new System.Windows.Forms.ProgressBar();
            this.labelSelectHint = new System.Windows.Forms.Label();
            this.listBoxSymbols = new System.Windows.Forms.ListBox();
            this.labelFilter = new System.Windows.Forms.Label();
            this.textBoxFilter = new System.Windows.Forms.TextBox();
            this.buttonRequestSymbols = new System.Windows.Forms.Button();
            this.buttonConnect = new System.Windows.Forms.Button();
            this.groupBoxStep2 = new System.Windows.Forms.GroupBox();
            this.groupBoxTrade = new System.Windows.Forms.GroupBox();
            this.labelPlaceOrderError = new System.Windows.Forms.Label();
            this.labelPrice = new System.Windows.Forms.Label();
            this.textBoxPrice = new System.Windows.Forms.TextBox();
            this.labelQuantityConst = new System.Windows.Forms.Label();
            this.buttonBuyLimit = new System.Windows.Forms.Button();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.buttonBuyMarket = new System.Windows.Forms.Button();
            this.buttonSellLimit = new System.Windows.Forms.Button();
            this.buttonSellMarket = new System.Windows.Forms.Button();
            this.labelActiveSymbol = new System.Windows.Forms.Label();
            this.labelActiveSymbolConst = new System.Windows.Forms.Label();
            this.labelLastTradeValue = new System.Windows.Forms.Label();
            this.labelLastTradeConst = new System.Windows.Forms.Label();
            this.listViewMarketDepth = new System.Windows.Forms.ListView();
            this.groupBoxStep3 = new System.Windows.Forms.GroupBox();
            this.labelCancelHint = new System.Windows.Forms.Label();
            this.labelStatusValue = new System.Windows.Forms.Label();
            this.labelStatus = new System.Windows.Forms.Label();
            this.buttonMassOrderStatusRequest = new System.Windows.Forms.Button();
            this.listViewOrders = new System.Windows.Forms.ListView();
            this.groupBoxStep4 = new System.Windows.Forms.GroupBox();
            this.checkBoxAutoUpdateAS = new System.Windows.Forms.CheckBox();
            this.buttonReloadAS = new System.Windows.Forms.Button();
            this.listViewAccountSummary = new System.Windows.Forms.ListView();
            this.buttonDisconnect = new System.Windows.Forms.Button();
            this.labelConnectionStatus = new System.Windows.Forms.Label();
            this.labelConnectionFeedStatusValue = new System.Windows.Forms.Label();
            this.labelConnectionBrokerStatus = new System.Windows.Forms.Label();
            this.labelConnectionBrokerStatusValue = new System.Windows.Forms.Label();
            this.groupBoxConnect = new System.Windows.Forms.GroupBox();
            this.groupBoxStep1.SuspendLayout();
            this.groupBoxStep2.SuspendLayout();
            this.groupBoxTrade.SuspendLayout();
            this.groupBoxStep3.SuspendLayout();
            this.groupBoxStep4.SuspendLayout();
            this.groupBoxConnect.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxStep1
            // 
            this.groupBoxStep1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.groupBoxStep1.Controls.Add(this.labelShownCount);
            this.groupBoxStep1.Controls.Add(this.labelShown);
            this.groupBoxStep1.Controls.Add(this.progressBarLoadSymbols);
            this.groupBoxStep1.Controls.Add(this.labelSelectHint);
            this.groupBoxStep1.Controls.Add(this.listBoxSymbols);
            this.groupBoxStep1.Controls.Add(this.labelFilter);
            this.groupBoxStep1.Controls.Add(this.textBoxFilter);
            this.groupBoxStep1.Controls.Add(this.buttonRequestSymbols);
            this.groupBoxStep1.Location = new System.Drawing.Point(12, 107);
            this.groupBoxStep1.Name = "groupBoxStep1";
            this.groupBoxStep1.Size = new System.Drawing.Size(218, 477);
            this.groupBoxStep1.TabIndex = 0;
            this.groupBoxStep1.TabStop = false;
            this.groupBoxStep1.Text = "Step 1: Request Symbols List and Choose symbol.";
            // 
            // labelShownCount
            // 
            this.labelShownCount.AutoSize = true;
            this.labelShownCount.Location = new System.Drawing.Point(55, 390);
            this.labelShownCount.Name = "labelShownCount";
            this.labelShownCount.Size = new System.Drawing.Size(45, 13);
            this.labelShownCount.TabIndex = 4;
            this.labelShownCount.Text = "0 from 0";
            // 
            // labelShown
            // 
            this.labelShown.AutoSize = true;
            this.labelShown.Location = new System.Drawing.Point(6, 390);
            this.labelShown.Name = "labelShown";
            this.labelShown.Size = new System.Drawing.Size(43, 13);
            this.labelShown.TabIndex = 3;
            this.labelShown.Text = "Shown:";
            // 
            // progressBarLoadSymbols
            // 
            this.progressBarLoadSymbols.Location = new System.Drawing.Point(6, 446);
            this.progressBarLoadSymbols.Name = "progressBarLoadSymbols";
            this.progressBarLoadSymbols.Size = new System.Drawing.Size(178, 23);
            this.progressBarLoadSymbols.TabIndex = 0;
            // 
            // labelSelectHint
            // 
            this.labelSelectHint.Location = new System.Drawing.Point(3, 416);
            this.labelSelectHint.Name = "labelSelectHint";
            this.labelSelectHint.Size = new System.Drawing.Size(178, 27);
            this.labelSelectHint.TabIndex = 2;
            this.labelSelectHint.Text = "Double click on symbol to subscribe for quotes";
            // 
            // listBoxSymbols
            // 
            this.listBoxSymbols.FormattingEnabled = true;
            this.listBoxSymbols.Location = new System.Drawing.Point(6, 94);
            this.listBoxSymbols.Name = "listBoxSymbols";
            this.listBoxSymbols.Size = new System.Drawing.Size(178, 290);
            this.listBoxSymbols.TabIndex = 1;
            this.listBoxSymbols.DoubleClick += new System.EventHandler(this.listBoxSymbols_DoubleClick);
            // 
            // labelFilter
            // 
            this.labelFilter.AutoSize = true;
            this.labelFilter.Location = new System.Drawing.Point(6, 72);
            this.labelFilter.Name = "labelFilter";
            this.labelFilter.Size = new System.Drawing.Size(32, 13);
            this.labelFilter.TabIndex = 1;
            this.labelFilter.Text = "Filter:";
            // 
            // textBoxFilter
            // 
            this.textBoxFilter.Location = new System.Drawing.Point(38, 69);
            this.textBoxFilter.Name = "textBoxFilter";
            this.textBoxFilter.Size = new System.Drawing.Size(146, 20);
            this.textBoxFilter.TabIndex = 1;
            this.textBoxFilter.TextChanged += new System.EventHandler(this.textBoxFilter_TextChanged);
            // 
            // buttonRequestSymbols
            // 
            this.buttonRequestSymbols.Location = new System.Drawing.Point(6, 31);
            this.buttonRequestSymbols.Name = "buttonRequestSymbols";
            this.buttonRequestSymbols.Size = new System.Drawing.Size(178, 23);
            this.buttonRequestSymbols.TabIndex = 1;
            this.buttonRequestSymbols.Text = "Request Symbols List";
            this.buttonRequestSymbols.UseVisualStyleBackColor = true;
            this.buttonRequestSymbols.Click += new System.EventHandler(this.buttonRequestSymbols_Click);
            // 
            // buttonConnect
            // 
            this.buttonConnect.Location = new System.Drawing.Point(6, 19);
            this.buttonConnect.Name = "buttonConnect";
            this.buttonConnect.Size = new System.Drawing.Size(148, 23);
            this.buttonConnect.TabIndex = 0;
            this.buttonConnect.Text = "Connect To EXANTE FIX";
            this.buttonConnect.UseVisualStyleBackColor = true;
            this.buttonConnect.Click += new System.EventHandler(this.buttonConnect_Click);
            // 
            // groupBoxStep2
            // 
            this.groupBoxStep2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.groupBoxStep2.Controls.Add(this.groupBoxTrade);
            this.groupBoxStep2.Controls.Add(this.labelActiveSymbol);
            this.groupBoxStep2.Controls.Add(this.labelActiveSymbolConst);
            this.groupBoxStep2.Controls.Add(this.labelLastTradeValue);
            this.groupBoxStep2.Controls.Add(this.labelLastTradeConst);
            this.groupBoxStep2.Controls.Add(this.listViewMarketDepth);
            this.groupBoxStep2.Location = new System.Drawing.Point(236, 12);
            this.groupBoxStep2.Name = "groupBoxStep2";
            this.groupBoxStep2.Size = new System.Drawing.Size(267, 572);
            this.groupBoxStep2.TabIndex = 1;
            this.groupBoxStep2.TabStop = false;
            this.groupBoxStep2.Text = "Step2: Check Quotes and Place Order.";
            // 
            // groupBoxTrade
            // 
            this.groupBoxTrade.Controls.Add(this.labelPlaceOrderError);
            this.groupBoxTrade.Controls.Add(this.labelPrice);
            this.groupBoxTrade.Controls.Add(this.textBoxPrice);
            this.groupBoxTrade.Controls.Add(this.labelQuantityConst);
            this.groupBoxTrade.Controls.Add(this.buttonBuyLimit);
            this.groupBoxTrade.Controls.Add(this.textBoxQuantity);
            this.groupBoxTrade.Controls.Add(this.buttonBuyMarket);
            this.groupBoxTrade.Controls.Add(this.buttonSellLimit);
            this.groupBoxTrade.Controls.Add(this.buttonSellMarket);
            this.groupBoxTrade.Location = new System.Drawing.Point(9, 441);
            this.groupBoxTrade.Name = "groupBoxTrade";
            this.groupBoxTrade.Size = new System.Drawing.Size(252, 131);
            this.groupBoxTrade.TabIndex = 11;
            this.groupBoxTrade.TabStop = false;
            this.groupBoxTrade.Text = "Place Order";
            // 
            // labelPlaceOrderError
            // 
            this.labelPlaceOrderError.AutoSize = true;
            this.labelPlaceOrderError.ForeColor = System.Drawing.Color.Red;
            this.labelPlaceOrderError.Location = new System.Drawing.Point(6, 115);
            this.labelPlaceOrderError.Name = "labelPlaceOrderError";
            this.labelPlaceOrderError.Size = new System.Drawing.Size(0, 13);
            this.labelPlaceOrderError.TabIndex = 15;
            // 
            // labelPrice
            // 
            this.labelPrice.AutoSize = true;
            this.labelPrice.Location = new System.Drawing.Point(139, 31);
            this.labelPrice.Name = "labelPrice";
            this.labelPrice.Size = new System.Drawing.Size(34, 13);
            this.labelPrice.TabIndex = 14;
            this.labelPrice.Text = "Price:";
            // 
            // textBoxPrice
            // 
            this.textBoxPrice.Location = new System.Drawing.Point(179, 28);
            this.textBoxPrice.Name = "textBoxPrice";
            this.textBoxPrice.Size = new System.Drawing.Size(62, 20);
            this.textBoxPrice.TabIndex = 13;
            // 
            // labelQuantityConst
            // 
            this.labelQuantityConst.AutoSize = true;
            this.labelQuantityConst.Location = new System.Drawing.Point(6, 31);
            this.labelQuantityConst.Name = "labelQuantityConst";
            this.labelQuantityConst.Size = new System.Drawing.Size(49, 13);
            this.labelQuantityConst.TabIndex = 12;
            this.labelQuantityConst.Text = "Quantity:";
            // 
            // buttonBuyLimit
            // 
            this.buttonBuyLimit.Location = new System.Drawing.Point(142, 57);
            this.buttonBuyLimit.Name = "buttonBuyLimit";
            this.buttonBuyLimit.Size = new System.Drawing.Size(99, 23);
            this.buttonBuyLimit.TabIndex = 7;
            this.buttonBuyLimit.Text = "Buy Limit";
            this.buttonBuyLimit.UseVisualStyleBackColor = true;
            this.buttonBuyLimit.Click += new System.EventHandler(this.buttonBuyLimit_Click);
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.Location = new System.Drawing.Point(58, 28);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(47, 20);
            this.textBoxQuantity.TabIndex = 10;
            // 
            // buttonBuyMarket
            // 
            this.buttonBuyMarket.Location = new System.Drawing.Point(9, 57);
            this.buttonBuyMarket.Name = "buttonBuyMarket";
            this.buttonBuyMarket.Size = new System.Drawing.Size(96, 23);
            this.buttonBuyMarket.TabIndex = 6;
            this.buttonBuyMarket.Text = "Buy Market";
            this.buttonBuyMarket.UseVisualStyleBackColor = true;
            this.buttonBuyMarket.Click += new System.EventHandler(this.buttonBuyMarket_Click);
            // 
            // buttonSellLimit
            // 
            this.buttonSellLimit.Location = new System.Drawing.Point(142, 87);
            this.buttonSellLimit.Name = "buttonSellLimit";
            this.buttonSellLimit.Size = new System.Drawing.Size(99, 23);
            this.buttonSellLimit.TabIndex = 9;
            this.buttonSellLimit.Text = "Sell Limit";
            this.buttonSellLimit.UseVisualStyleBackColor = true;
            this.buttonSellLimit.Click += new System.EventHandler(this.buttonSellLimit_Click);
            // 
            // buttonSellMarket
            // 
            this.buttonSellMarket.Location = new System.Drawing.Point(9, 86);
            this.buttonSellMarket.Name = "buttonSellMarket";
            this.buttonSellMarket.Size = new System.Drawing.Size(96, 23);
            this.buttonSellMarket.TabIndex = 8;
            this.buttonSellMarket.Text = "Sell Market";
            this.buttonSellMarket.UseVisualStyleBackColor = true;
            this.buttonSellMarket.Click += new System.EventHandler(this.buttonSellMarket_Click);
            // 
            // labelActiveSymbol
            // 
            this.labelActiveSymbol.AutoSize = true;
            this.labelActiveSymbol.Location = new System.Drawing.Point(56, 31);
            this.labelActiveSymbol.Name = "labelActiveSymbol";
            this.labelActiveSymbol.Size = new System.Drawing.Size(0, 13);
            this.labelActiveSymbol.TabIndex = 5;
            // 
            // labelActiveSymbolConst
            // 
            this.labelActiveSymbolConst.AutoSize = true;
            this.labelActiveSymbolConst.Location = new System.Drawing.Point(6, 31);
            this.labelActiveSymbolConst.Name = "labelActiveSymbolConst";
            this.labelActiveSymbolConst.Size = new System.Drawing.Size(44, 13);
            this.labelActiveSymbolConst.TabIndex = 4;
            this.labelActiveSymbolConst.Text = "Symbol:";
            // 
            // labelLastTradeValue
            // 
            this.labelLastTradeValue.AutoSize = true;
            this.labelLastTradeValue.Location = new System.Drawing.Point(73, 420);
            this.labelLastTradeValue.Name = "labelLastTradeValue";
            this.labelLastTradeValue.Size = new System.Drawing.Size(0, 13);
            this.labelLastTradeValue.TabIndex = 3;
            // 
            // labelLastTradeConst
            // 
            this.labelLastTradeConst.AutoSize = true;
            this.labelLastTradeConst.Location = new System.Drawing.Point(6, 420);
            this.labelLastTradeConst.Name = "labelLastTradeConst";
            this.labelLastTradeConst.Size = new System.Drawing.Size(61, 13);
            this.labelLastTradeConst.TabIndex = 2;
            this.labelLastTradeConst.Text = "Last Trade:";
            // 
            // listViewMarketDepth
            // 
            this.listViewMarketDepth.Location = new System.Drawing.Point(6, 52);
            this.listViewMarketDepth.Name = "listViewMarketDepth";
            this.listViewMarketDepth.Size = new System.Drawing.Size(255, 365);
            this.listViewMarketDepth.TabIndex = 1;
            this.listViewMarketDepth.UseCompatibleStateImageBehavior = false;
            this.listViewMarketDepth.View = System.Windows.Forms.View.Details;
            // 
            // groupBoxStep3
            // 
            this.groupBoxStep3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.groupBoxStep3.Controls.Add(this.labelCancelHint);
            this.groupBoxStep3.Controls.Add(this.labelStatusValue);
            this.groupBoxStep3.Controls.Add(this.labelStatus);
            this.groupBoxStep3.Controls.Add(this.buttonMassOrderStatusRequest);
            this.groupBoxStep3.Controls.Add(this.listViewOrders);
            this.groupBoxStep3.Location = new System.Drawing.Point(512, 12);
            this.groupBoxStep3.Name = "groupBoxStep3";
            this.groupBoxStep3.Size = new System.Drawing.Size(588, 277);
            this.groupBoxStep3.TabIndex = 2;
            this.groupBoxStep3.TabStop = false;
            this.groupBoxStep3.Text = "Step 3: Control Active Orders";
            // 
            // labelCancelHint
            // 
            this.labelCancelHint.AutoSize = true;
            this.labelCancelHint.Location = new System.Drawing.Point(9, 244);
            this.labelCancelHint.Name = "labelCancelHint";
            this.labelCancelHint.Size = new System.Drawing.Size(113, 13);
            this.labelCancelHint.TabIndex = 17;
            this.labelCancelHint.Text = "Double click to cancel";
            // 
            // labelStatusValue
            // 
            this.labelStatusValue.AutoSize = true;
            this.labelStatusValue.Location = new System.Drawing.Point(524, 244);
            this.labelStatusValue.Name = "labelStatusValue";
            this.labelStatusValue.Size = new System.Drawing.Size(51, 13);
            this.labelStatusValue.TabIndex = 16;
            this.labelStatusValue.Text = "unknown";
            // 
            // labelStatus
            // 
            this.labelStatus.AutoSize = true;
            this.labelStatus.Location = new System.Drawing.Point(483, 244);
            this.labelStatus.Name = "labelStatus";
            this.labelStatus.Size = new System.Drawing.Size(40, 13);
            this.labelStatus.TabIndex = 15;
            this.labelStatus.Text = "Status:";
            // 
            // buttonMassOrderStatusRequest
            // 
            this.buttonMassOrderStatusRequest.Location = new System.Drawing.Point(299, 239);
            this.buttonMassOrderStatusRequest.Name = "buttonMassOrderStatusRequest";
            this.buttonMassOrderStatusRequest.Size = new System.Drawing.Size(178, 23);
            this.buttonMassOrderStatusRequest.TabIndex = 14;
            this.buttonMassOrderStatusRequest.Text = "Mass Order Status Request";
            this.buttonMassOrderStatusRequest.UseVisualStyleBackColor = true;
            this.buttonMassOrderStatusRequest.Click += new System.EventHandler(this.buttonMassOrderStatusRequest_Click);
            // 
            // listViewOrders
            // 
            this.listViewOrders.FullRowSelect = true;
            this.listViewOrders.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewOrders.HideSelection = false;
            this.listViewOrders.Location = new System.Drawing.Point(12, 19);
            this.listViewOrders.MultiSelect = false;
            this.listViewOrders.Name = "listViewOrders";
            this.listViewOrders.Size = new System.Drawing.Size(570, 214);
            this.listViewOrders.TabIndex = 13;
            this.listViewOrders.UseCompatibleStateImageBehavior = false;
            this.listViewOrders.View = System.Windows.Forms.View.Details;
            this.listViewOrders.DoubleClick += new System.EventHandler(this.listViewOrders_DoubleClick);
            // 
            // groupBoxStep4
            // 
            this.groupBoxStep4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.groupBoxStep4.Controls.Add(this.checkBoxAutoUpdateAS);
            this.groupBoxStep4.Controls.Add(this.buttonReloadAS);
            this.groupBoxStep4.Controls.Add(this.listViewAccountSummary);
            this.groupBoxStep4.Location = new System.Drawing.Point(512, 295);
            this.groupBoxStep4.Name = "groupBoxStep4";
            this.groupBoxStep4.Size = new System.Drawing.Size(588, 289);
            this.groupBoxStep4.TabIndex = 3;
            this.groupBoxStep4.TabStop = false;
            this.groupBoxStep4.Text = "Step 4: Check Account Summary";
            // 
            // checkBoxAutoUpdateAS
            // 
            this.checkBoxAutoUpdateAS.AutoSize = true;
            this.checkBoxAutoUpdateAS.Location = new System.Drawing.Point(201, 255);
            this.checkBoxAutoUpdateAS.Name = "checkBoxAutoUpdateAS";
            this.checkBoxAutoUpdateAS.Size = new System.Drawing.Size(115, 17);
            this.checkBoxAutoUpdateAS.TabIndex = 14;
            this.checkBoxAutoUpdateAS.Text = "AutoUpdate (1sec)";
            this.checkBoxAutoUpdateAS.UseVisualStyleBackColor = true;
            // 
            // buttonReloadAS
            // 
            this.buttonReloadAS.Location = new System.Drawing.Point(6, 251);
            this.buttonReloadAS.Name = "buttonReloadAS";
            this.buttonReloadAS.Size = new System.Drawing.Size(178, 23);
            this.buttonReloadAS.TabIndex = 13;
            this.buttonReloadAS.Text = "Request Account Summary";
            this.buttonReloadAS.UseVisualStyleBackColor = true;
            this.buttonReloadAS.Click += new System.EventHandler(this.buttonReloadAS_Click);
            // 
            // listViewAccountSummary
            // 
            this.listViewAccountSummary.Location = new System.Drawing.Point(6, 19);
            this.listViewAccountSummary.Name = "listViewAccountSummary";
            this.listViewAccountSummary.Size = new System.Drawing.Size(576, 219);
            this.listViewAccountSummary.TabIndex = 12;
            this.listViewAccountSummary.UseCompatibleStateImageBehavior = false;
            this.listViewAccountSummary.View = System.Windows.Forms.View.Details;
            // 
            // buttonDisconnect
            // 
            this.buttonDisconnect.Location = new System.Drawing.Point(6, 48);
            this.buttonDisconnect.Name = "buttonDisconnect";
            this.buttonDisconnect.Size = new System.Drawing.Size(69, 23);
            this.buttonDisconnect.TabIndex = 4;
            this.buttonDisconnect.Text = "Disconnect";
            this.buttonDisconnect.UseVisualStyleBackColor = true;
            this.buttonDisconnect.Click += new System.EventHandler(this.buttonDisconnect_Click);
            // 
            // labelConnectionStatus
            // 
            this.labelConnectionStatus.AutoSize = true;
            this.labelConnectionStatus.Location = new System.Drawing.Point(81, 49);
            this.labelConnectionStatus.Name = "labelConnectionStatus";
            this.labelConnectionStatus.Size = new System.Drawing.Size(67, 13);
            this.labelConnectionStatus.TabIndex = 16;
            this.labelConnectionStatus.Text = "Feed Status:";
            // 
            // labelConnectionFeedStatusValue
            // 
            this.labelConnectionFeedStatusValue.AutoSize = true;
            this.labelConnectionFeedStatusValue.Location = new System.Drawing.Point(154, 49);
            this.labelConnectionFeedStatusValue.Name = "labelConnectionFeedStatusValue";
            this.labelConnectionFeedStatusValue.Size = new System.Drawing.Size(51, 13);
            this.labelConnectionFeedStatusValue.TabIndex = 17;
            this.labelConnectionFeedStatusValue.Text = "unknown";
            // 
            // labelConnectionBrokerStatus
            // 
            this.labelConnectionBrokerStatus.AutoSize = true;
            this.labelConnectionBrokerStatus.Location = new System.Drawing.Point(81, 68);
            this.labelConnectionBrokerStatus.Name = "labelConnectionBrokerStatus";
            this.labelConnectionBrokerStatus.Size = new System.Drawing.Size(67, 13);
            this.labelConnectionBrokerStatus.TabIndex = 18;
            this.labelConnectionBrokerStatus.Text = "Exec Status:";
            // 
            // labelConnectionBrokerStatusValue
            // 
            this.labelConnectionBrokerStatusValue.AutoSize = true;
            this.labelConnectionBrokerStatusValue.Location = new System.Drawing.Point(154, 68);
            this.labelConnectionBrokerStatusValue.Name = "labelConnectionBrokerStatusValue";
            this.labelConnectionBrokerStatusValue.Size = new System.Drawing.Size(51, 13);
            this.labelConnectionBrokerStatusValue.TabIndex = 19;
            this.labelConnectionBrokerStatusValue.Text = "unknown";
            // 
            // groupBoxConnect
            // 
            this.groupBoxConnect.BackColor = System.Drawing.Color.Silver;
            this.groupBoxConnect.Controls.Add(this.buttonConnect);
            this.groupBoxConnect.Controls.Add(this.labelConnectionBrokerStatusValue);
            this.groupBoxConnect.Controls.Add(this.buttonDisconnect);
            this.groupBoxConnect.Controls.Add(this.labelConnectionBrokerStatus);
            this.groupBoxConnect.Controls.Add(this.labelConnectionFeedStatusValue);
            this.groupBoxConnect.Controls.Add(this.labelConnectionStatus);
            this.groupBoxConnect.Location = new System.Drawing.Point(12, 12);
            this.groupBoxConnect.Name = "groupBoxConnect";
            this.groupBoxConnect.Size = new System.Drawing.Size(218, 89);
            this.groupBoxConnect.TabIndex = 20;
            this.groupBoxConnect.TabStop = false;
            this.groupBoxConnect.Text = "Step 0: Connect to EXANTE FIX";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1104, 591);
            this.Controls.Add(this.groupBoxConnect);
            this.Controls.Add(this.groupBoxStep4);
            this.Controls.Add(this.groupBoxStep3);
            this.Controls.Add(this.groupBoxStep2);
            this.Controls.Add(this.groupBoxStep1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.ShowIcon = false;
            this.Text = "FIXTradingDemo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.groupBoxStep1.ResumeLayout(false);
            this.groupBoxStep1.PerformLayout();
            this.groupBoxStep2.ResumeLayout(false);
            this.groupBoxStep2.PerformLayout();
            this.groupBoxTrade.ResumeLayout(false);
            this.groupBoxTrade.PerformLayout();
            this.groupBoxStep3.ResumeLayout(false);
            this.groupBoxStep3.PerformLayout();
            this.groupBoxStep4.ResumeLayout(false);
            this.groupBoxStep4.PerformLayout();
            this.groupBoxConnect.ResumeLayout(false);
            this.groupBoxConnect.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxStep1;
        private System.Windows.Forms.ListBox listBoxSymbols;
        private System.Windows.Forms.Label labelFilter;
        private System.Windows.Forms.TextBox textBoxFilter;
        private System.Windows.Forms.Button buttonRequestSymbols;
        private System.Windows.Forms.Label labelSelectHint;
        private System.Windows.Forms.GroupBox groupBoxStep2;
        private System.Windows.Forms.GroupBox groupBoxStep3;
        private System.Windows.Forms.GroupBox groupBoxStep4;
        private System.Windows.Forms.Button buttonConnect;
        private System.Windows.Forms.ProgressBar progressBarLoadSymbols;
        private System.Windows.Forms.Label labelShownCount;
        private System.Windows.Forms.Label labelShown;
        private System.Windows.Forms.ListView listViewMarketDepth;
        private System.Windows.Forms.Label labelLastTradeValue;
        private System.Windows.Forms.Label labelLastTradeConst;
        private System.Windows.Forms.Label labelActiveSymbol;
        private System.Windows.Forms.Label labelActiveSymbolConst;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.Button buttonSellLimit;
        private System.Windows.Forms.Button buttonSellMarket;
        private System.Windows.Forms.Button buttonBuyLimit;
        private System.Windows.Forms.Button buttonBuyMarket;
        private System.Windows.Forms.GroupBox groupBoxTrade;
        private System.Windows.Forms.Label labelPrice;
        private System.Windows.Forms.TextBox textBoxPrice;
        private System.Windows.Forms.Label labelQuantityConst;
        private System.Windows.Forms.Label labelPlaceOrderError;
        private System.Windows.Forms.ListView listViewAccountSummary;
        private System.Windows.Forms.CheckBox checkBoxAutoUpdateAS;
        private System.Windows.Forms.Button buttonReloadAS;
        private System.Windows.Forms.ListView listViewOrders;
        private System.Windows.Forms.Button buttonMassOrderStatusRequest;
        private System.Windows.Forms.Button buttonDisconnect;
        private System.Windows.Forms.Label labelStatusValue;
        private System.Windows.Forms.Label labelStatus;
        private System.Windows.Forms.Label labelCancelHint;
        private System.Windows.Forms.Label labelConnectionStatus;
        private System.Windows.Forms.Label labelConnectionFeedStatusValue;
        private System.Windows.Forms.Label labelConnectionBrokerStatus;
        private System.Windows.Forms.Label labelConnectionBrokerStatusValue;
        private System.Windows.Forms.GroupBox groupBoxConnect;
    }
}

